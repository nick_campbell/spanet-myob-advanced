using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using PX.Common;
using PX.Data;
using PX.Objects.CA;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.SO;
using PX.SM;
using PX.Objects.AR.CCPaymentProcessing;
using PX.Objects.Common;
using CashAccountAttribute = PX.Objects.GL.CashAccountAttribute;
using PX.Objects;
using PX.Objects.AR;

namespace PX.Objects.AR
{
  	public class CustomerMaint_Extension:PXGraphExtension<CustomerMaint>
  	{
		#region Event Handlers
  
	    public PXAction<Customer> OpenDocuments; 
	    [PXUIField(DisplayName = "Open Documents")] 
	    [PXButton] 
      
    	protected virtual IEnumerable openDocuments(PXAdapter adapter) 
    	{     
	     	Customer customer = Base.BAccount.Current;
	     	string sURL = String.Format("~/genericinquiry/genericinquiry.aspx?id=36f069c6-cc37-4e71-a98c-56326745998e&Customer={0}", customer.AcctCD);     
	    	// throw new PXRedirectToUrlException(sURL,null); 
	     	throw new PXRedirectToUrlException(sURL, PXBaseRedirectException.WindowMode.New, false, null);
		}
	       
	    
	    public override void Initialize() 
		{   
	    	base. Initialize();   
	    	Base.inquiry.AddMenuAction(OpenDocuments) ;   
	   	} 
	
		#endregion
	}
}